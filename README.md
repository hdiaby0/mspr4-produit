# Projet API Produit

Ce projet, "API Produit", nécessite l'ouverture dans IntelliJ IDEA.
## Docker et base de données

Ce projet repose sur une base de données Docker. Vous pouvez trouver les fichiers nécessaires ici : [docker-database](https://github.com/yacoubhrm/docker-database.git). Suivez les instructions d'installation disponibles sur le dépôt Git.

Après avoir lancé la base de données, veuillez installer les dépendances et lancer le projet en exécutant la commande suivante dans le fichier `pom.xml` :
```bash
mvn clean install
Commandes de test
Une fois le projet lancé, vous pouvez effectuer des tests en utilisant les commandes suivantes :

Supprimer un produit :
bash
Copier le code
Invoke-WebRequest -Uri http://localhost:8080/products/1 -Method DELETE
Mettre à jour un produit :
bash
Copier le code
Invoke-WebRequest -Uri http://localhost:8081/products/1 -Method PUT -Body '{"name":"Produit A Modifié", "price":89.99, "availability":15}' -ContentType "application/json"
Ajouter un nouveau produit :
bash
Copier le code
Invoke-WebRequest -Uri http://localhost:8081/products -Method POST -Body '{"name":"Produit A", "price":99.99, "availability":10}' -ContentType "application/json"
Obtenir un produit par son ID :
bash
Copier le code
Invoke-WebRequest -Uri http://localhost:8081/products/1 -Method GET
Obtenir tous les produits :
bash
Copier le code
Invoke-WebRequest -Uri http://localhost:8081/products -Method GET

