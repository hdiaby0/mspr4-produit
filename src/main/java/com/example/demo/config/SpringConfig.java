package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class SpringConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.demo.controller")) // Correction du package
                .paths(PathSelectors.ant("/products/**")) // Ajustement du chemin pour refléter vos endpoints
                .build()
                .apiInfo(apiInfo()) // Ajout de l'ApiInfo dans la configuration du Docket
                .securitySchemes(Arrays.asList(apiKey())) // Utilisation de Arrays.asList() au lieu de List.of()
                .securityContexts(Arrays.asList(securityContext())); // Utilisation de Arrays.asList() au lieu de List.of()
    }

    private ApiKey apiKey() {
        return new ApiKey("basicAuth", "Authorization", "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(new SecurityReference("basicAuth", authorizationScopes)); // Utilisation de Arrays.asList() au lieu de List.of()
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "My REST API", // titre
                "Some custom description of API.", // description
                "Version 1.0", // version
                "Terms of service", // termes de service
                new Contact("Maxence EYMERY", "www.example.com", "maxenceeymery2002@gmail.com"),
                "License of API", "API license URL", Collections.emptyList());
    }
}
